<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="competences.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Goblin+One&display=swap" rel="stylesheet"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php include 'databis.php'; ?>




    <div class="competence">
        <h1>C1:Maquetter une application</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C1"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>


    </div>

    <div class="competence">
        <h1>C2:Réaliser une interface web  statique et adaptable</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C2"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>

    </div>

    <div class="competence">
        <h1>C3:Réaliser une interface utilisateur web dynamique</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C3"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>

    <div class="competence">
        <h1>C4:Réaliser une interface web utilisateur avec une solution de gestion de contenu ou e-commerce</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C4"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>

    <div class="competence">
        <h1>C5:Créer une base de données</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C5"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>

    <div class="competence">
        <h1>C6:Développer les composants d'accès aux données</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C6"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>

    <div class="competence">
        <h1>C7:développer la partie back-end d'une application web ou web mobile</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C7"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>

    <div class="competence">
        <h1>C8:élaborer et mettre en oeuvre des composants dans une application de gestion de contenu ou e-commerce</h1>
        <img src="" alt="">
        <div class="monlien">
            <?php
            $comp = '"C8"';
            $mesliensbycomp = getlienbycomp($comp);
            foreach ($mesliensbycomp as $lienbycomp) { ?>

                <a href="<?php echo $lienbycomp['liencode'] ?>">mon lien</a>

            <?php } ?>
        </div>
    </div>



</body>

</html>