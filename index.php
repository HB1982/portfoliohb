<!DOCTYPE html>
<html lang="en">

<head>
    <link href="style.css" rel="stylesheet">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Shadows+Into+Light&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>


    <div id="index">
        <video playsinline autoplay muted loop>
            <source src="IMAGES/Ocean - 69949.mp4" type="video/mp4">
        </video>
        <div class="indexmenu">
            <ul>
                <li> <a class="btn btn-primary" href="#index">Home</a></li>
                <li><a class="btn btn-primary" href="#presentation">Presentation</a></li>
                <li><a class="btn btn-primary" href="#projets">Mon travail</a></li>
                <li><a class="btn btn-primary" href="#competences">Compétences</a></li>
                <li><a class="btn btn-primary" href="#contact">Contacts</a></li>


            </ul>
        </div>
        <p class="intro">Hélène BEURY DEVELOPPEUSE WEB</p>

    </div>
    <div class="gestion">
        <a href="add.php">Ajouter un projet</a>
        <a href="supprimer.php">supprimer un projet</a>
    </div>



    <div id="presentation">
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ab mollitia maxime minus cum doloremque consectetur ullam quia possimus tempore deserunt eaque distinctio alias laboriosam vitae, consequuntur omnis repellendus adipisci nobis!
            Non alias laudantium accusantium, ea quia vitae. Autem fuga molestiae repellendus quidem modi excepturi. Sapiente autem libero sit unde quae maxime amet consequatur, est placeat provident, numquam, vero vel necessitatibus.
            Quibusdam corporis est magnam ducimus! Tempore nostrum officiis odio? Perspiciatis cupiditate harum optio earum nobis cum, quod ducimus dignissimos totam enim nesciunt mollitia laboriosam voluptatem maiores neque voluptas magni minima!
            Id aut fugiat quis in assumenda aperiam eius perspiciatis distinctio deserunt suscipit? Dolore officiis, ut deserunt distinctio consequatur assumenda beatae, at eum repellat architecto similique! Perspiciatis, explicabo esse? Itaque, mollitia.
            Veniam, quae saepe ex tempore quod commodi animi accusantium voluptatum itaque necessitatibus cupiditate! Vitae debitis maiores quas nisi? Quisquam earum reprehenderit porro magnam ducimus voluptatibus provident aliquid illo perspiciatis enim!
        </p>
    </div>


    <div id="projets">

        <nav>
            <ul class="menu">
                <li>JAVASCRIPT</li>
                <li>PHP</li>
                <li>REACT</li>
                <li>SYMFONY</li>
                <li>WORDPRESS</li>
            </ul>
        </nav>

        <?php include 'data.php';
        $list_projets = getallprojets(); ?>
        <?php foreach ($list_projets as $projet) {  ?>
            <div>
                <div class="realisations">
                    <img src="upload/<?php echo $projet['imageprojet']; ?>" alt="photoprojet">
                    <div class="presenteprojet">
                        <h1><?php echo $projet['nom']; ?></h1>
                        <p><?php echo $projet['synopsis']; ?></p>
                        <a href="<?php echo $projet['lien']; ?>">lien</a>

                        <?php $list_competences = namecompprojet($projet['projet_id']);
                        foreach ($list_competences as $competences) { ?>
                            <p><?php echo $competences['nom']; ?></p>
                        <?php } ?>

                        <?php $list_tech = nametechnoprojet($projet['projet_id']);
                        foreach ($list_tech as $tech) { ?>
                            <p><?php echo $tech['nom']; ?></p>
                        <?php } ?>



                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
    <div id="competences">

        <div class="container">
            <div class="cell cell-1"> <a href="competences.php#C1">C1</a></div>
            <div class="cell cell-2"><a href="competences.php#C2">C2</a></div>
            <div class="cell cell-3"><a href="competences.php#C3">C3</a></div>
            <div class="cell cell-4"><a href="competences.php#C4">C4</a></div>
            <div class="cell cell-5"><a href="competences.php#C5">C5</a></div>
            <div class="cell cell-6"><a href="competences.php#C6">C6</a></div>
            <div class="cell cell-7"><a href="competences.php#C7">C7</a></div>
            <div class="cell cell-8"><a href="competences.php#C8">C8</a></div>
            <div class="cell cell-9"><a href="competences.php#C9">C9</a></div>
        </div>
    </div>


    <section id="contacts">




        <form action="mail.php" method="post">
            <h1>Contact</h1>
            <label for="first_name">Prénom</label>
            <input type="text" name="first_name">
            <label for="last_name">Nom</label>
            <input type="text" name="last_name">
            <label for="email">Email</label>
            <input type="text" name="email">
            <label for="message">Votre message</label>
            <textarea rows="5" name="message" cols="30"></textarea>
            <input type="submit" name="submit" value="Submit">
        </form>


    </section>


    <footer>
        <a href=""></a>
        <p>Copyrights</p>

    </footer>

</body>


<script type="text/javascript" src="file.js"></script>



</html>