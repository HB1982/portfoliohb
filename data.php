<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

 $host = "localhost";  
 $username = "helene";  
 $password = "BEURY";  

 
$connect = new PDO("mysql:host=$host; dbname=PORTFOLIO", $username, $password);  

function getallprojets(){
global $connect;
$query = $connect-> query ("SELECT * FROM PROJETS ");

return $query->fetchAll();
}

function namecompprojet($id_du_projet){
    global $connect;
    $query= $connect-> prepare ("
    SELECT COMPETENCES.nom 
    FROM COMPETENCES, PROJETS_COMPETENCES 
    WHERE COMPETENCES.comp_id= PROJETS_COMPETENCES.comp_id 
    AND PROJETS_COMPETENCES.projet_id=?
    " 
    );
    $query->execute([$id_du_projet]);
    return $query -> fetchAll();
}

    
function insertprojet($imageprojet, $nom, $synopsis, $lien, $comps,$techs){
    global $connect;
    $query = $connect->prepare("
INSERT INTO PROJETS (imageprojet,nom,synopsis,lien)
        VALUES(
        ?,
        ?,
        ?,
        ?
        )"
    );
    $query->execute([$imageprojet,$nom,$synopsis,$lien]);
    
    $id_proj = $connect->lastInsertId();
   
    foreach($comps as $id_comp){
        insertid($id_proj, $id_comp);
    }
   
    foreach($techs as $id_tech){
        insertidtech($id_proj,$id_tech);
    }
}

function insertid($id_PROJETS,$id_COMPETENCES){
    global $connect;
    $query = $connect->prepare("INSERT INTO PROJETS_COMPETENCES(projet_id, comp_id) VALUES (?,?);");
    $query->execute([$id_PROJETS,$id_COMPETENCES]);
}

function insertidtech($id_PROJETSTECH,$id_TECH){
    global $connect;
    $query = $connect->prepare("INSERT INTO PROJETS_TECHNOS(projettech_id, tech_id) VALUES (?,?);");
    $query->execute([$id_PROJETSTECH,$id_TECH]);
}

function nametechnoprojet($id_du_projet){
    global $connect;
    $query= $connect-> prepare ("
    SELECT TECHNO.nom 
    FROM TECHNO, PROJETS_TECHNOS 
    WHERE TECHNO.tech_id= PROJETS_TECHNOS.tech_id 
    AND PROJETS_TECHNOS.projettech_id=?
    " 
    );
    $query->execute([$id_du_projet]);
    return $query -> fetchAll();
}
